package gamemap

type Cavern struct {
	above, below *Cavern
	depth        int
}

func (c Cavern) Above() *Cavern {
	return c.above
}

func (c Cavern) Below() *Cavern {
	return c.below
}

func (c Cavern) Depth() int {
	return c.depth
}

func (c Cavern) IsBaseCamp() bool {
	return c.above == nil
}

func (c Cavern) IsDeepest() bool {
	return c.below == nil
}

func (c *Cavern) Expand() {
	c.below = &Cavern{
		above: c,
		depth: c.depth + 20,
	}
}
