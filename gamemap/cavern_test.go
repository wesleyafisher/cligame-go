package gamemap

import "testing"

func TestExpand(t *testing.T) {
	cavern := Cavern{}
	cavern.Expand()
	if cavern.below == nil || cavern.below.depth == 0 {
		t.Log("Cavern did not expand")
		t.Fail()
	}
}

func BenchmarkExpand(b *testing.B) {
    cavern := Cavern{}
    for i := 0; i < b.N; i++ {
        cavern.Expand()
    }
}
