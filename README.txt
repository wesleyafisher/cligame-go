Go commands
go fmt ./... - format files
go test ./... - run tests
go test ./... -bench . - run benchmarks
go build - compile files
go run . - run files
go clean - remove build artifacts

Changelog
Commit | Build time | Run time | Binary size | Comment
1      | ?          | ?        | ?           | Initialize project and print out welcome message
2      | 0m0.181s   | 0m0.001s | 2.1M        | Setup REPL with commands help and logout
3      | 0m0.214s   | 0m0.001s | 2.1M        | Initialize player and map and the ability to ascend and descend
4      | 0m0.253s   | 0m0.001s | 2.1M        | Refactor: Move command processing to seperate module
5      | 0m0.177s   | 0m0.002s | 2.1M        | Refactor: Make cavern details private and add test / benchmark
