package main

import (
	"bufio"
	parser "cligame/commandParser"
	"cligame/constants"
	gameplayer "cligame/player"
	"fmt"
	"os"
)

func main() {
	fmt.Printf("Loading player... ")
	player := gameplayer.LoadPlayer()
	fmt.Printf("done\n")

	fmt.Printf("Welcome to %v. Type 'help' to get more information.\n",
		constants.GAME_NAME)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		parser.ParseCommand(&player, scanner.Text())
	}
}
