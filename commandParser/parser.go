package commandParser

import (
	"cligame/constants"
	"cligame/player"
	"fmt"
	"os"
)

func ParseCommand(player *player.Player, command string) {
	switch command {
	case "help":
		help()
	case "logout":
		os.Exit(0)
	case "location":
		player.PrintLocation()
	case "ascend":
		player.Ascend()
	case "descend":
		player.Descend()
	default:
		fmt.Printf("%v is not a valid command\n", command)
		help()
	}
}

func help() {
	fmt.Printf("help: display this message\n")
	fmt.Printf("logout: stop playing %v\n", constants.GAME_NAME)
	fmt.Printf("location: display your current location\n")
	fmt.Printf("ascend: head back the way you came\n")
	fmt.Printf("descend: head deeper into the cave\n")
}
