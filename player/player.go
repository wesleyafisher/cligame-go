package player

import (
	"cligame/gamemap"
	"fmt"
)

type Player struct {
	location *gamemap.Cavern
}

func (p Player) PrintLocation() {
	if location := p.location; location.IsBaseCamp() {
		fmt.Printf("You are at base camp\n")
	} else {
		fmt.Printf("You are %v feet below the surface\n", location.Depth())
	}
}

func (p *Player) Ascend() {
	if p.location.IsBaseCamp() {
		fmt.Printf("You are already at base camp!\n")
		return
	}

	fmt.Printf("You head back the way you came...\n")
	p.location = p.location.Above()
	p.PrintLocation()
}

func (p *Player) Descend() {
	if p.location.IsBaseCamp() {
		fmt.Printf("You head into the cave...\n")
	} else {
		fmt.Printf("You head deeper into the cave...\n")
	}

	if p.location.IsDeepest() {
		p.location.Expand()
	}

	p.location = p.location.Below()
	p.PrintLocation()
}

func LoadPlayer() Player {
	return Player{new(gamemap.Cavern)}
}
